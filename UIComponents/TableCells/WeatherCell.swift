//
//  WeatherCell.swift
//  UTair
//
//  Created by Евгений Елчев on 29.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

public final class WeatherCell: UITableViewCell {
    
    private static let flightDateFormat = "HH:mm"
    private static let flightDateFormater: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = flightDateFormat
        return df
    }()

    public func configure(temp: Double, date: Double) {
        textLabel?.text = String(format: "%0.f C", temp)
        let date = Date(timeIntervalSince1970: date)
        detailTextLabel?.text = WeatherCell.flightDateFormater.string(from: date)
    }

}
