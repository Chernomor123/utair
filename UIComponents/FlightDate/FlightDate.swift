//
//  FlightDate.swift
//  UTair
//
//  Created by Евгений Елчев on 26.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

@IBDesignable
public final class FlightDate: XibView {
    @IBOutlet public weak var hintLabelView: UILabel!
    @IBOutlet public weak var dateLabelView: UILabel!
}


// MARK: - IBInspectable wrappers
public extension FlightDate {
    
    @objc @IBInspectable
    public var hintLabel: String? {
        get {
            return hintLabelView.text
        }
        set {
            hintLabelView.text = newValue
        }
    }
    
    @objc @IBInspectable
    public var dateLabel: String? {
        get {
            return dateLabelView.text
        }
        set {
            dateLabelView.text = newValue
        }
    }
    
}
