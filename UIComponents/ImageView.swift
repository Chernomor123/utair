//
//  ImageView.swift
//  UTair
//
//  Created by Евгений Елчев on 26.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit


/// Workaround bag http://openradar.appspot.com/18448072
public class ImageView: UIImageView {

    public override func awakeFromNib() {
        super.awakeFromNib()
        tintColorDidChange()
    }

}
