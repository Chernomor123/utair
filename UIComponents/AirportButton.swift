//
//  AirportButton.swift
//  UTair
//
//  Created by Евгений Елчев on 26.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

// Кнопка с дополнительной надписью,
// во время работы над проектом я подумал, что кнопка не очень подходит, но
// так как это не прод, я ее оставил

// FIXME: Изменить кнопку на просто uiview
@IBDesignable
public final class AirportButton: UIButton {
    
    private static let airportFont = UIFont.systemFont(ofSize: 10, weight: .light)
    private static let airportDefaultColor = UIColor.white
    
    private var pAirportLabelBootomContraint: NSLayoutConstraint?
    
    private let pAirportLabel: UILabel = {
        let label = UILabel()
        label.font = airportFont
        label.textColor = airportDefaultColor
        return label
    }()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        makeAirportLabel()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        makeAirportLabel()
    }
    
    // заголовок перемещется наверх кнопки
    public override func titleRect(forContentRect contentRect: CGRect) -> CGRect {
        let defaultRect = super.titleRect(forContentRect: contentRect)
        let origin = CGPoint(x: defaultRect.origin.x, y: contentEdgeInsets.top)
        return CGRect(origin: origin, size: defaultRect.size)
    }
    
    private func makeAirportLabel() {
        addSubview(pAirportLabel)
        makeAirportLabelConstraints()
    }
    
    private func makeAirportLabelConstraints() {
        pAirportLabel.translatesAutoresizingMaskIntoConstraints = false
        pAirportLabelBootomContraint = pAirportLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        pAirportLabelBootomContraint?.isActive = true
        pAirportLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        pAirportLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    }
    
    public override var contentEdgeInsets: UIEdgeInsets { didSet {
        setNeedsUpdateConstraints()
    }}
    
    public override func updateConstraints() {
        super.updateConstraints()
        pAirportLabelBootomContraint?.constant = contentEdgeInsets.bottom * -1
    }
    
}

// MARK: - IBInspectable wrappers
public extension AirportButton {
    
    @objc @IBInspectable
    public var airportTextColor: UIColor {
        get {
            return pAirportLabel.textColor
        }
        set {
            pAirportLabel.textColor = newValue
        }
    }
    
    @objc @IBInspectable
    public var airportText: String? {
        get {
            return pAirportLabel.text
        }
        set {
            pAirportLabel.text = newValue
        }
    }
    
}
