//
//  City.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

struct City: Codable {
    let country: String
    let name: String
    
    enum CodingKeys: String, CodingKey {
        case name = "city"
        case country
    }
}
