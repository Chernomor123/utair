//
//  Weather.swift
//  UTair
//
//  Created by Евгений Елчев on 29.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

struct Weather: Codable {
    let date: Double
    let main: WeatherMain
    
    enum CodingKeys: String, CodingKey {
        case date = "dt"
        case main
    }
}

struct WeatherMain: Codable {
    let temp: Double
}
