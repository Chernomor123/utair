//
//  AbstractBuilder.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation
import Swinject

// Так как в сторибоард без магии не сделать иньекцию зависимостей
// этим будут заниматься строители модулей
// Они инкапсулируют внутри себя асмеблер от Swinject, что бы не размазывать его
// по проекту
class AbstractModuleBuilder: NSObject {
    
    // В настоящих проектах несколько асембелров, которые объеденяются в один
    // на весь проект один асемблер отвечающий за сборку сущностей,
    // его можно создать в AppDelegate, что бы он строил граф сущностей
    // Но при использовании IB(который сам по себе DI), эта система рушится
    // Оптимально создавать его в абстрактном классе с приватным доступом
    //
    // Если бы при этом наш проект нужнадался в создании сервисов вне модулей,
    // Например в то же AppDelegate, то абстрактный билдер можно было перенести
    // в файл с делегатом. Уровень доступа при этом изменится на fileprivate.
    private static let assembler = Assembler([ServiceAssembly()])
    
    //убираем дублирвоание кода и за одно внедряем наш асемблер в сборщик модуля
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configure(assembler: AbstractModuleBuilder.assembler)
    }
    
    //абстрактный метод сборки модуля
    func configure(assembler: Assembler) {}
}
