//
//  CityResult.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

struct CityResult: Codable {
    let cityes: [City]
    
    enum CodingKeys: String, CodingKey {
        case cityes = "results"
    }
}
