//
//  CityNetworkServiceImp.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation
import Alamofire

final class CityNetworkServiceImp: CityNetworkService {
    
    private let router: CityRouter
    private let sessionManager: SessionManager
    let queue = DispatchQueue.global(qos: .userInitiated)
    let decoder = JSONDecoder()
    
    init(router: CityRouter, sessionManager: SessionManager) {
        self.router = router
        self.sessionManager = sessionManager
    }
    
    /// Загрузка списка городов
    ///
    /// - Parameters:
    ///   - query: поисковый запрос, ищет в имени города и почтовом индексе, может быть пустым
    func search(query: String, succes: @escaping AllSucces, fail: @escaping AllFail) {
        sessionManager.request(router.search(query: query)).responseData(queue: queue) { [weak self] reponse in
            
            switch reponse.result {
            case .success(let data):
                self?.praceAllCity(data: data, succes: succes, fail: fail)
            case .failure(let error):
                assertionFailure(error.localizedDescription)
                fail(error)
            }
        }
    }
    
    private func praceAllCity(data: Data, succes: @escaping AllSucces, fail: @escaping AllFail) {
        do {
            let result = try decoder.decode(CityResult.self, from: data)
            DispatchQueue.main.async {
                succes(result.cityes)
            }
        } catch {
            // Сервис часто возвращает пустую строку с данными,
            // на продакшене я бы добавил кэш в базу, что бы избежать частых
            // ошибок поиска
            assertionFailure(error.localizedDescription)
            DispatchQueue.main.async {
                fail(error)
            }
        }
    }
    
}
