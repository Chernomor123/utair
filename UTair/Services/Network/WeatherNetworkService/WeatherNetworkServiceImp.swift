//
//  WeatherNetworkServiceImp.swift
//  UTair
//
//  Created by Евгений Елчев on 29.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation
import Alamofire

final class WeatherNetworkServiceImp: WeatherNetworkService {

    private let router: WeatherApiRouter
    private let sessionManager: SessionManager
    private let queue = DispatchQueue.global(qos: .userInitiated)
    private let decoder = JSONDecoder()
    
    init(router: WeatherApiRouter, sessionManager: SessionManager) {
        self.router = router
        self.sessionManager = sessionManager
    }
    
    ///загрузка погоды на 5 дней с интервалом в 3 часа
    func week(city: City, succes: @escaping Succes, fail: @escaping Fail) {
        let request = router.week(city: city.name, country: city.country)
        sessionManager.request(request).responseData(queue: queue) { [weak self] reponse in
            switch reponse.result {
            case .success(let data):
                self?.parseAllWeather(data: data, succes: succes, fail: fail)
            case .failure(let error):
                assertionFailure(error.localizedDescription)
                fail(error)
            }
        }
    }
    
    private func parseAllWeather(data: Data, succes: @escaping Succes, fail: @escaping Fail) {
        do {
            let result = try decoder.decode(WeatherResult.self, from: data)
            DispatchQueue.main.async {
                succes(result.weathers)
            }
        } catch {
            assertionFailure(error.localizedDescription)
            DispatchQueue.main.async {
                fail(error)
            }
        }
    }
    
}
