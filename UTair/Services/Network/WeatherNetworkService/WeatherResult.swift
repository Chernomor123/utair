//
//  WeatherResult.swift
//  UTair
//
//  Created by Евгений Елчев on 29.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

//Структура парсинга ответа от погодного сервиса
struct WeatherResult: Codable {
    let weathers: [Weather]
    
    enum CodingKeys: String, CodingKey {
        case weathers = "list"
    }
}
