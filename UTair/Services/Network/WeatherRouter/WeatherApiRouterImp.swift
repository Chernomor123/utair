//
//  WeatherRouterImp.swift
//  UTair
//
//  Created by Евгений Елчев on 29.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation
import Alamofire

struct WeatherApiRouterImp: WeatherApiRouter {
    private let enviroment: Enviromentable

    init(enviroment: Enviromentable) {
        self.enviroment = enviroment
    }

    /// Запрос для получения подгодных данных за 5 дней с интервалом в 3 часа
    ///
    /// - Parameter city: Название города
    func week(city: String, country: String) -> URLRequestConvertible {
        return Week(baseUrl: enviroment.weatherBaseApiUrl, apiKey: enviroment.weatherApiKey, city: city, country: country)
    }

    private struct Week: RequestRoute {
        let baseUrl: URL?
        let apiKey: String
        let city: String
        let country: String

        var method: HTTPMethod = .get
        
        var path: String = "data/2.5/forecast"

        // не знаю на сколько можно считать указание `metric` в словаре
        // "волшебной строкой"
        // принцип единственого источника истины не нарушен
        // читается легко, можно конечно завернуть в enum с возможными
        // вариантами, но YAGNI не советует)
        
        // units - еденицы измерения в которых возвращается температура,
        // metric - градусы
        var parameters: Parameters {
            return [
                "q": String.init(format: "%@,%@", city, country),
                "units": "metric",
                "appid": apiKey
            ]
        }

        init(baseUrl: URL?, apiKey: String, city: String, country: String) {
            self.baseUrl = baseUrl
            self.apiKey = apiKey
            self.city = city
            self.country = country
        }
    }
}

