//
//  WeatherRouter.swift
//  UTair
//
//  Created by Евгений Елчев on 29.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation
import Alamofire

protocol WeatherApiRouter {
    func week(city: String, country: String) -> URLRequestConvertible
}
