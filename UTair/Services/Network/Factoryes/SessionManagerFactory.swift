//
//  SessionManagerFactory.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation
import Alamofire

protocol SessionManagerFactory {
    func `default`() -> SessionManager
}
