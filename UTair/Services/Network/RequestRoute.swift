//
//  RequestRoute.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation
import Alamofire

// Определяет builder для URLRequest
// Позволяет стандартизировать их создание и улучить читаемость и изменение
protocol RequestRoute: URLRequestConvertible {
    var baseUrl: URL? { get }
    var method: HTTPMethod { get }
    var path: String { get }
    var parameters: Parameters { get }
    var fullUrl: URL? { get }
}

extension RequestRoute {
    var fullUrl: URL? {
        return baseUrl?.appendingPathComponent(path)
    }
    
    func asURLRequest() throws -> URLRequest {
        guard let url = fullUrl else { throw AFError.invalidURL(url: path) }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        
        return try URLEncoding.default.encode(urlRequest, with: parameters)
    }
}
