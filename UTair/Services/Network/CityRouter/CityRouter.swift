//
//  CityRouter.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation
import Alamofire

protocol CityRouter {
    func search(query: String) -> URLRequestConvertible
}
