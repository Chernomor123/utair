//
//  KeyboadManager.swift
//  MyPin
//
//  Created by Евгений Елчев on 17.03.17.
//  Copyright © 2017 Евгений Елчев. All rights reserved.
//

import Foundation

import UIKit

final class KeyboadManager {
    
    init() {}

    var scrollView: UIScrollView?

    func startKeyboardListen() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWasShown), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }

    func endKeyboardListen() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    @objc func keyboardWasShown(notification: Notification) {
        guard let info = notification.userInfo as NSDictionary?,
            let value = info.value(forKey: UIKeyboardFrameEndUserInfoKey) as? NSValue else {
                assertionFailure()
                return
        }
        
        let kbSize = value.cgRectValue.size
        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0)

        self.scrollView?.contentInset = contentInsets
        scrollView?.scrollIndicatorInsets = contentInsets
    }

    @objc func keyboardWillBeHidden(notification: Notification) {
        let contentInsets = UIEdgeInsets.zero
        scrollView?.contentInset = contentInsets
        scrollView?.scrollIndicatorInsets = contentInsets
    }

}
