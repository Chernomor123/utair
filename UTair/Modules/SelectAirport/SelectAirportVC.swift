//
//  SelectAirportVC.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

protocol SelectAirportVCDelegat: class {
    func onAirportSelect(city: City, point: Point)
}

protocol SelectAirportVC: class {
    var delegate: SelectAirportVCDelegat? { get set }
    func setStartState(point: Point, city: City?)
}
