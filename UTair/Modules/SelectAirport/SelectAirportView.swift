//
//  SelectAirportView.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

protocol SelectAirportView: class {
    var tableView: UITableView! { get }
    var searchFeld: UITextField! { get }
    var airportLabelView: UILabel! { get }
    var searchActivity: UIActivityIndicatorView! { get }
    func set(point: Point)
    func set(city: City?)
}
