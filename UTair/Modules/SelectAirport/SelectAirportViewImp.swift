//
//  SelectAirportViewImp.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

final class SelectAirportViewImp: UIView, SelectAirportView {
    // Такая же надписать дублируется в другом View, но есть вероятность, что они
    // будут изменяться независимо
    private let defaultCountryLabel = "Все aропорты"
    
    @IBOutlet weak var searchFeld: UITextField!
    @IBOutlet weak var airportLabelView: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchActivity: UIActivityIndicatorView!
    
    func set(point: Point) {
        searchFeld.placeholder = point.rawValue
    }
    
    func set(city: City?) {
        searchFeld.text = city?.name ?? ""
        airportLabelView.text = city?.country ?? defaultCountryLabel
    }
    
    override func awakeFromNib() {
        searchFeld.becomeFirstResponder()
    }
}
