//
//  SelectAirportTableManager.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit
import UIComponents

final class SelectAirportTableManagerImp: NSObject, SelectAirportTableManager {
    weak var tableView: UITableView?
    var cityes = [City]() { didSet {
        tableView?.reloadData()
    }}
    
    var onAirportSelect: OnAirportSelect?
    
    typealias Cell = AirportCell
    private let cellReuseId = String(describing: Cell.self)
    private let cellNibName = String(describing: Cell.self)
    
    init(tableView: UITableView) {
        super.init()
        self.tableView = tableView
        
        registerNib()
        setDelegate()
    }
    
    private func registerNib() {
        let bundle = Bundle(for: Cell.self)
        let nib = UINib(nibName: cellNibName, bundle: bundle)
        tableView?.register(nib, forCellReuseIdentifier: cellReuseId)
    }
    
    private func setDelegate() {
        tableView?.dataSource = self
        tableView?.delegate = self
    }
}


extension SelectAirportTableManagerImp: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cityes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId, for: indexPath) as? Cell else {
            assertionFailure()
            return UITableViewCell()
        }
        
        let item = cityes[indexPath.row]
        cell.configure(name: item.name, country: item.country)
        
        return cell
    }
}

extension SelectAirportTableManagerImp: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let city = cityes[indexPath.row]
        onAirportSelect?(city)
    }
}
