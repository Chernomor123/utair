//
//  CalendarVC.swift
//  UTair
//
//  Created by Евгений Елчев on 28.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

protocol CalendarVC: class {
    typealias OnDateSelect = (Date) -> Void
    var minimalDate: Date { get set }
    var onDateSelect: OnDateSelect? { get set }
}
