//
//  BuyTiketsFormManagerImp.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

// Менеджер формы поиска рейсов
final class BuyTiketsFormManagerImp: BuyTiketsFormManager {
    
    // парметры валидации формы
    // разница должна быть в сутках
    private let minimalDifferentBetweenFlight: TimeInterval = 60 * 60 * 24
    
    weak var view: BuyTiketsView!
    var onDepartureAitportButtonPressed: OnAitportButtonPressed?
    var onArrivalAitportButtonPressed: OnAitportButtonPressed?
    
    var onThitherwardDatePressed: OnDatePressed?
    var onBackwardDatePressed: OnDatePressed?
    
    var onFindButtonPressed: OnFindButtonPressed?
    
    
    var state = FindRequest(
        arrival: Flight(airport: nil, date: nil),
        departure: Flight(airport: nil, date: Date()),
        hummanAmount: (adult: 1, kid: 0, baby: 0)
    )
    
    init(view: BuyTiketsView) {
        self.view = view
        updateView()
        
        view.departureAitportButton.addTarget(self, action: #selector(onDepartureAitportButtonPressed(sender:)), for: .touchUpInside)
        view.arrivalAitportButton.addTarget(self, action: #selector(onArrivalAitportButtonPressed(sender:)), for: .touchUpInside)
        view.swapAitportButton.addTarget(self, action: #selector(onSwapAirportPressed(sender:)), for: .touchUpInside)
        view.findFlightButton.addTarget(self, action: #selector(onFindButtonPressed(sender:)), for: .touchUpInside)
        
        let thitherwardGesture = UITapGestureRecognizer(target: self, action: #selector(ThitherwardDatePressed))
        view.thitherwardDateView.addGestureRecognizer(thitherwardGesture)
        let backwardGesture = UITapGestureRecognizer(target: self, action: #selector(backwardDatePressed))
        view.backwardDateWrapperView.addGestureRecognizer(backwardGesture)
        
        view.adultStepperView.delegate = self
        view.kidStepperView.delegate = self
        view.babyStepperView.delegate = self
    }
    
    func updateView() {
        view.setArrivalAirport(name: state.arrival.airport?.name, country: state.arrival.airport?.country)
        view.setDepartureAirport(name: state.departure.airport?.name, country: state.departure.airport?.country)
        view.setThitherwardDate(state.departure.date)
        view.setbackwardDate(state.arrival.date)
        view.adultStepperView.amount = state.hummanAmount.adult
        view.kidStepperView.amount = state.hummanAmount.kid
        view.babyStepperView.amount = state.hummanAmount.baby
    }
    
    private func clearbackwardDate() {
        state.arrival.date = nil
        view.setbackwardDate(nil)
    }
    
    private func setThitherward(date: Date?) {
        if let date = date, let backwardDate = state.arrival.date, date >= backwardDate {
            clearbackwardDate()
        }
        state.departure.date = date
        view.setThitherwardDate(date)
    }
    
    @objc func onDepartureAitportButtonPressed(sender: UIButton) {
        onDepartureAitportButtonPressed?(.departure, self, state.departure.airport)
    }
    
    @objc func onArrivalAitportButtonPressed(sender: UIButton) {
        onArrivalAitportButtonPressed?(.arrival, self, state.arrival.airport)
    }
    
    @objc
    func ThitherwardDatePressed() {
        let minimumDate = Date()
        onThitherwardDatePressed?(minimumDate) { [weak self] date in
            self?.setThitherward(date: date)
        }
    }
    
    @objc
    func backwardDatePressed() {
        let willClear = state.arrival.date != nil
        if willClear {
            clearbackwardDate()
            return
        }
        
        var minimumDate = state.departure.date ?? Date()
        minimumDate = minimumDate.addingTimeInterval(minimalDifferentBetweenFlight)
        onBackwardDatePressed?(minimumDate) { [weak self] date in
            self?.state.arrival.date = date
            self?.view.setbackwardDate(date)
        }
    }
    
    @objc func onSwapAirportPressed(sender: UIButton) {
        let arrivalAirport = state.arrival.airport
        state.arrival.airport = state.departure.airport
        state.departure.airport = arrivalAirport
        view.setDepartureAirport(name: state.departure.airport?.name, country: state.departure.airport?.country)
        view.setArrivalAirport(name: state.arrival.airport?.name, country: state.arrival.airport?.country)
    }
    
    @objc
    func onFindButtonPressed(sender: UIButton) {
        guard let fromAirport = state.departure.airport,
            let toAirport = state.arrival.airport,
            state.departure.date != nil,
            state.arrival.date != nil else {
                view.showError(withMessage: FormError.emptyForm.rawValue)
            return
        }
        
        onFindButtonPressed?(fromAirport, toAirport)
        
    }

}
