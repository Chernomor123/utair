//
//  BuyTiketsFormManager.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

protocol BuyTiketsFormManager {
    typealias OnAitportButtonPressed = (Point, SelectAirportVCDelegat, City?) -> Void
    typealias OnDatePressed = (Date, @escaping CalendarVC.OnDateSelect) -> Void?
    typealias OnFindButtonPressed = (City, City) -> Void
    
    var onArrivalAitportButtonPressed: OnAitportButtonPressed? { get set }
    var onDepartureAitportButtonPressed: OnAitportButtonPressed? { get set }
    
    var onThitherwardDatePressed: OnDatePressed? { get set }
    var onBackwardDatePressed: OnDatePressed? { get set }
    
    var onFindButtonPressed: OnFindButtonPressed? { get set }
    
}
