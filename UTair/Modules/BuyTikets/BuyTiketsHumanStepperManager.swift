//
//  BuyTiketsHumanStepperManager.swift
//  UTair
//
//  Created by Евгений Елчев on 30.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation
import UIComponents

private let maxHummanAmount = 9
private let minimumAdultHummanAmount = 1
private let minimumInadultHummanAmount = 0

//Обрабатывает выбор количества людей
extension BuyTiketsFormManagerImp: HumanStepperDelegate {
    func humanStepper(_ stepper: HumanStepper, shouldDisableMinusButtonAtValue value: Int) -> Bool {
        if stepper == view.adultStepperView && value <= minimumAdultHummanAmount {
            return true
        } else if value <= minimumInadultHummanAmount {
            return true
        } else {
            return false
        }
    }
    
    func humanStepper(_ stepper: HumanStepper, shouldChangeFromValue fromValue: Int, toValue: Int) -> Bool {
        let upOperation = toValue > fromValue
        
        // Куча условий, если степер работает не верно, прочите коментарии
        // и сможете понять, ошибся я в коде или просто неучел какой то кейс
        
        // Это просто, есди мы пытаемся поставить взрослых пасажиров меньше 1го
        // то файл, дети и младенцы не могут летать одни
        if !upOperation && stepper == view.adultStepperView && toValue < minimumAdultHummanAmount {
            return false
            // Ну и разумеется нельзя поставить отрицательное количество детей или
            // младенцев
        } else if !upOperation && toValue < minimumInadultHummanAmount {
            return false
            // Дальше сложнее
            // Если у нас уже набралось максимальное количество пасажиров для заказа
            // то фаейл
        } else if upOperation && state.totalHummanAmount == maxHummanAmount {
            view.showError(withMessage: FormError.humanOverflow.rawValue)
            return false
            // Если мы пытаемся провести больше младенцев чем взрослых то фейл
            // младенец должен сидеть на руках у взрослых
        } else if upOperation && stepper == view.babyStepperView && state.hummanAmount.adult <= fromValue {
            view.showError(withMessage: FormError.babyOverflow.rawValue)
            return false
            // Тут фейла нет, но есть условие. Если мы уменьшаем количество взрослых
            // билетов и билетов для младнцев становится больше то мы уменьшаем
            // колличество билетов для младенцев иначе выходит баг позволяющий
            // обойти предыдущее условие
        } else if !upOperation && stepper == view.adultStepperView && toValue < state.hummanAmount.baby {
            view.babyStepperView.amount = toValue
            return true
        } else {
            return true
        }
        
    }
    
    func humanStepper(_ stepper: HumanStepper, didChangeValue value: Int) {
        switch stepper {
        case view.adultStepperView:
            state.hummanAmount.adult = value
        case view.kidStepperView:
            state.hummanAmount.kid = value
        case view.babyStepperView:
            state.hummanAmount.baby = value
        default:
            assertionFailure()
            break
        }
        
    }
    
}
