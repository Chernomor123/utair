//
//  BuyTiketsFindRequest.swift
//  UTair
//
//  Created by Евгений Елчев on 30.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

extension BuyTiketsFormManagerImp {
    struct Flight {
        var airport: City?
        var date: Date?
    }
    
    struct FindRequest {
        var arrival: Flight
        var departure: Flight
        var hummanAmount: (adult: Int, kid: Int, baby: Int)
        
        var totalHummanAmount: Int {
            return hummanAmount.adult + hummanAmount.kid + hummanAmount.baby
        }
    }
}
