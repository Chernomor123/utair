//
//  BuyTiketsView.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit
import UIComponents

protocol BuyTiketsView: class {
    var departureAitportButton: AirportButton! { get}
    var arrivalAitportButton: AirportButton! { get}
    var swapAitportButton: UIButton! { get}
    
    var thitherwardDateView: FlightDate! { get}
    var backwardDateWrapperView: BorderedView! { get }
    
    var adultStepperView: HumanStepper! { get}
    var kidStepperView: HumanStepper! { get}
    var babyStepperView: HumanStepper! { get}
    var findFlightButton: BorderedButton! { get }
    
    func setThitherwardDate(_ date: Date?)
    func setbackwardDate(_ date: Date?)
    func showError(withMessage message: String)
    func setArrivalAirport(name: String?, country: String?)
    func setDepartureAirport(name: String?, country: String?)
}
