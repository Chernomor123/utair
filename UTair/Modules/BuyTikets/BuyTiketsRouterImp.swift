//
//  BuyTiketsRouterImp.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

final class BuyTiketsRouterImp: BuyTiketsRouter {

    struct SegueID {
        static let onAirportButtonPressed = "onAirportButtonPressed"
        static let onDateButtonPressed = "onDateButtonPressed"
        static let onFindButtonPressed = "onFindButtonPressed"
    }
    
    private lazy var mapPrepareToSegue = [
        SegueID.onAirportButtonPressed: goToSelectArport,
        SegueID.onDateButtonPressed: goToCalendar,
        SegueID.onFindButtonPressed: goToWeather
    ]
    
    weak var vc: UIViewController?
    init(vc: UIViewController) {
        self.vc = vc
    }
    
    func onAitportButtonPressed(point: Point, delegate: SelectAirportVCDelegat, city: City?) {
        vc?.performSegue(withIdentifier: SegueID.onAirportButtonPressed, sender: (point, delegate, city))
    }
    
    func onDateButtonPressed(minimumDate: Date, completion: @escaping CalendarVC.OnDateSelect) {
        vc?.performSegue(withIdentifier: SegueID.onDateButtonPressed, sender: (minimumDate, completion))
    }
    
    func onFindButtonPressed(from: City, to: City) {
        
        vc?.performSegue(withIdentifier: SegueID.onFindButtonPressed, sender: (from, to))
    }
    
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier,
            let prepare = mapPrepareToSegue[identifier] else {
            assertionFailure()
            return
        }

        prepare(segue, sender)
        
    }
    
    private func goToSelectArport(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? SelectAirportVC else {
            assertionFailure()
            return
        }
        guard let (point, delegate, city) = sender as? (Point, SelectAirportVCDelegat, City?) else {
            assertionFailure()
            return
        }
        destination.setStartState(point: point, city: city)
        destination.delegate = delegate
    }
    
    private func goToCalendar(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? CalendarVC else {
            assertionFailure()
            return
        }
        guard let (date, completion) = sender as? (Date, CalendarVC.OnDateSelect) else {
            assertionFailure()
            return
        }
        destination.minimalDate = date
        destination.onDateSelect = completion
    }
    
    private func goToWeather(for segue: UIStoryboardSegue, sender: Any?) {
        guard let destination = segue.destination as? WeatherVC else {
            assertionFailure()
            return
        }
        guard let (from, to) = sender as? (City, City) else {
            assertionFailure()
            return
        }
        
        destination.cityes = (from: from, to: to)
    }
    
    
}
