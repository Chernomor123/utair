//
//  BuyTiketsRouter.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

protocol BuyTiketsRouter: class {
    func prepare(for segue: UIStoryboardSegue, sender: Any?)
    
    func onAitportButtonPressed(point: Point, delegate: SelectAirportVCDelegat, city: City?)
    func onDateButtonPressed(minimumDate: Date, completion: @escaping CalendarVC.OnDateSelect)
    func onFindButtonPressed(from: City, to: City)
}
