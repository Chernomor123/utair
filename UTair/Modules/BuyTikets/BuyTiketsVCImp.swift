//
//  BuyTiketsVCImp.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

final class BuyTiketsVCImp: UIViewController, BuyTiketsVC {
    var router: BuyTiketsRouter!
    var formManager: BuyTiketsFormManager!
    var customView: BuyTiketsView {
        return view as! BuyTiketsView
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formManager = BuyTiketsFormManagerImp(view: customView)
        weak var router = self.router
        formManager.onDepartureAitportButtonPressed = router?.onAitportButtonPressed
        formManager.onArrivalAitportButtonPressed = router?.onAitportButtonPressed
        
        formManager.onThitherwardDatePressed = router?.onDateButtonPressed
        formManager.onBackwardDatePressed = router?.onDateButtonPressed
        
        formManager.onFindButtonPressed = router?.onFindButtonPressed
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router.prepare(for: segue, sender: sender)
    }
    
}
