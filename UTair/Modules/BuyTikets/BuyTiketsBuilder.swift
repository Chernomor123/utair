//
//  BuyTiketsBuilder.swift
//  UTair
//
//  Created by Евгений Елчев on 27.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit
import Swinject

final class BuyTiketsBuilder: AbstractModuleBuilder {
    
    @IBOutlet weak var vc: BuyTiketsVCImp!
    
    override func configure(assembler: Assembler) {
        vc.router = BuyTiketsRouterImp(vc: vc)
    }
    
}
