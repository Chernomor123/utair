//
//  BuyTiketsFormError.swift
//  UTair
//
//  Created by Евгений Елчев on 30.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

extension BuyTiketsFormManagerImp {    
    enum FormError: String {
        case emptyForm = "Необходимо выбрать аэропорты и даты прибытия/отправления"
        case humanOverflow = "Пасажирова не должно быть больше 9"
        case babyOverflow = "Младенцев не должно быть больше чем взрослых"
    }
}
