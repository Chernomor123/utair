//
//  BuyTiketsAirportManager.swift
//  UTair
//
//  Created by Евгений Елчев on 30.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import Foundation

//Обрабатывает выбор аэропорта
extension BuyTiketsFormManagerImp: SelectAirportVCDelegat {
    func onAirportSelect(city: City, point: Point) {
        switch point {
        case .departure:
            state.departure.airport = city
            view.setDepartureAirport(name: city.name, country: city.country)
        case .arrival:
            state.arrival.airport = city
            view.setArrivalAirport(name: city.name, country: city.country)
        }
    }
}
