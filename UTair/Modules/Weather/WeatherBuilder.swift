//
//  WeatherBuilder.swift
//  UTair
//
//  Created by Евгений Елчев on 29.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit
import Swinject

final class WeatherBuilder: AbstractModuleBuilder {
    
    @IBOutlet weak var vc: WeatherVCImp!
    
    override func configure(assembler: Assembler) {
        if let weatherNetworkService  = assembler.resolver.resolve(WeatherNetworkService.self) {
            vc.weatherNetworkService = weatherNetworkService
        } else {
            assertionFailure()
        }
    }
    
}
