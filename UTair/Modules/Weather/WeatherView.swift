//
//  WeatherView.swift
//  UTair
//
//  Created by Евгений Елчев on 29.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

protocol WeatherView: class {

    var tableView: UITableView! { get }
    var fromTabButton: UIButton! { get }
    var toTabButton: UIButton! { get }
    
    var tabSelect: WeatherViewImp.Direction { get }
    
    func tabSelect(_ button: UIButton)
}
