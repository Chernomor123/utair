//
//  WeatherViewImp.swift
//  UTair
//
//  Created by Евгений Елчев on 29.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

final class WeatherViewImp: UIView, WeatherView {

    @IBOutlet weak var tableView: UITableView! { didSet {
        tableView.refreshControl = UIRefreshControl()
    }}
    @IBOutlet weak var fromTabButton: UIButton! { didSet {
        configTabImage(forButton: fromTabButton)
    }}
    @IBOutlet weak var toTabButton: UIButton! { didSet {
        configTabImage(forButton: toTabButton)
    }}
    
    private func configTabImage(forButton button: UIButton) {
        let image = button.image(for: .normal)?.withRenderingMode(.alwaysOriginal)
        button.setImage(image, for: .normal)
    }
    
    func tabSelect(_ button: UIButton) {
        fromTabButton.isSelected = button == fromTabButton
        toTabButton.isSelected = button == toTabButton
    }
    
    var tabSelect: Direction {
        if fromTabButton.isSelected {
            return .from
        } else if toTabButton.isSelected {
            return .to
        } else {
            assertionFailure()
            return .from
        }
    }
}

extension WeatherViewImp {
    enum Direction {
        case from, to
    }
}
