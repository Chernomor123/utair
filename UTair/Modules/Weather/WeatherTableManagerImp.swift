//
//  WeatherTableManagerimp.swift
//  UTair
//
//  Created by Евгений Елчев on 29.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit
import UIComponents


final class WeatherTableManagerImp: NSObject, WeatherTableManager {
    let cellReuseID = "WeatherCell"
    var weathers = WeatherStore() { didSet {
        tableView?.reloadData()
        tableView?.refreshControl?.endRefreshing()
    }}
    
    weak var tableView: UITableView?
    
    init(tableView: UITableView) {
        super.init()
        self.tableView = tableView
        self.tableView?.dataSource = self
    }
    
}


extension WeatherTableManagerImp: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return weathers.title(forSection: section)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return weathers.sectionCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weathers[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseID, for: indexPath) as? WeatherCell else {
            assertionFailure()
            return UITableViewCell()
        }
        let item = weathers[indexPath.section, indexPath.row]
        cell.configure(temp: item.main.temp, date: item.date)
        return cell
    }
}
