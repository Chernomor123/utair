//
//  ErrorMessage.swift
//  UTair
//
//  Created by Евгений Елчев on 28.10.2017.
//  Copyright © 2017 jonfit. All rights reserved.
//

import UIKit

// Хедпер для отображения ошибок
extension UIViewController {
    private static let title = "Ошибка"
    private static let buttonTitle = "Ок"
    typealias completion = (UIAlertAction) -> Void
    
    ///  Презентует UIAlertController с сообщением об ошибке
    ///
    /// - Parameters:
    ///   - message: текст ошибки
    ///   - animated: показывать с анимацией
    ///   - completion: замыкание выполняемое после нажатия кнопки 'ok'
    func showError(message: String, animated: Bool, completion: completion? = nil) {
        let ctrl = UIAlertController(title: UIViewController.title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: UIViewController.buttonTitle, style: .cancel, handler: completion)
        ctrl.addAction(action)
        present(ctrl, animated: animated, completion: nil)
    }
}


